import React from 'react';
import './App.css';

import Contact from './components/Contact';
import Coordinator from './components/Coordinator';
import Graduation from './components/Graduation';
import Interests from './components/Interests';
import Navigation from './components/Navigation';
import News from './components/News';
import Research from './components/Research';
import Student from './components/Student';

import Navbar from './components/Navbar';
import Landing from './components/Landing';
import Login from './components/Login';
import Register from './components/Register';
import UserFunctions from './components/UserFunctions';

import {
  BrowserRouter as Router,
  Switch, 
  Route
} from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar/>
        <Route path="/" exact component={Landing}/>
        <Switch>
          <Route path="/Contact" component={Contact} />
          <Route path="/Coordinator" component={Coordinator} /> 
          <Route path="/Graduation" component={Graduation} />
          <Route path="/Interests" component={Interests} />
          <Route path="/News" component={News} />
          <Route path="/Research" component={Research} />
          <Route path="/Student" component={Student} /> 
          <div className="container">
            <Route path="/register" component={Register} />
            <Route path="/login" component={Login} />
          </div>

        </Switch>
      </div>
    </Router>
  );
}



export default App;
