import axios from 'axios'

export const getList = () => {
    return axios
    .get('http://localhost:8000/api/comments' ,{
        headers: {'Content-Type': 'application/json'}
    })
    .then(res => {
        return res.data
    })
}

export const addComment = desc => {
    return axios
    .post('http://localhost:8000/api/comments',
    {
        desc:desc
    },
    {
        headers: {'Content-Type': 'application/json'}
    })
    .then(res => {
        console.log(res)
    })
}