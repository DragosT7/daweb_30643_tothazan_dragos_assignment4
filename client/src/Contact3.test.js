import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Landing from './components/Landing';

Enzyme.configure({ adapter: new Adapter() });

describe('Landing', () => {
    it('should show text', ()=>{
        const wrapper = shallow(<Contact/>);
        const text2 = wrapper.find('div div div h1');
        expect(text2.text()).toBe('Welcome to first');
    });
});
