import React, { Component } from 'react'
import { getList, addComment } from '../Comments'
import { Bar, Line, Pie } from 'react-chartjs-2';


class Comments extends Component {
    constructor() {
        super()
        this.state = {
            id: '',
            description: '',
            created_at: '',
            editDisabled: false,
            items: [],
            chartData: {
                labels: [],
                datasets: [
                    {
                        label: "",
                        data: [0],
                        backgroudColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ]
                    }
                ]
            }
        }

        this.onSubmit = this.onSubmit.bind(this)
        this.onChange = this.onChange.bind(this)
    }




    componentDidMount() {
        this.getAll()
    }

    onChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    getAll = () => {
        getList().then(data => {
            this.setState(
                {
                    description: '',
                    created_at: '',
                    items: [...data]
                },
                () => {
                    // console.log(this.state.items)
                }
            )
        })
    }

    onSubmit = e => {
        e.preventDefault()
        addComment(this.state.description).then(() => {
            this.getAll()
        })
        this.setState({
            description: ''
        })
    }


    onEdit = (itemid, e) => {
        e.preventDefault()

        var data = [...this.state.items]
        data.forEach((item, index) => {
            if (item.id === itemid) {
                this.setState({
                    id: item.id,
                    description: item.description,
                    created_at: item.created_at,
                    editDisabled: true
                })
            }
        })
    }


    render() {
        var countCommentsPerDay = {};
        var data2 = [...this.state.items];
        var x = new Date();
        let arr = [];
        let mySet = new Set();
        data2.forEach(element => {
            x = new Date(element.created_at);
            mySet.add(x.toLocaleDateString("en-GB"))
            if (x.toLocaleDateString("en-GB") in countCommentsPerDay) {
                countCommentsPerDay[x.toLocaleDateString("en-GB")] += 1;
            } else {
                countCommentsPerDay[x.toLocaleDateString("en-GB")] = 1;
            }
        })
        mySet.forEach(element => {
            this.state.chartData.labels.push(element);
            arr.push(countCommentsPerDay[element]);
            if (element == "13/05/2020") {
                this.state.chartData.datasets.push({ label: element, data: arr, backgroudColor: [] });
            }
        })

        return (
            <div className="chart">
                <Pie
                    data={this.state.chartData}
                    width={100}
                    height={50}
                    options={{}}
                />

            </div>
        )
    }
}

export default Comments