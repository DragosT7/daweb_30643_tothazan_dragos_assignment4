import React, { Component } from 'react';

async function read(content) {
  var file = await fetch("http://localhost:3000/news.xml");
  const text = await file.text();

  var XMLParser = await require('react-xml-parser');
  var xmlParsedFromString = await new XMLParser().parseFromString(text);

  return xmlParsedFromString.getElementsByTagName(content)[0].value;
}

class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: ''
    }
  }

  componentDidMount() {
    this.getContent();
  }

  getContent() {
    read('content').then(c => {
      this.setState({ content: c })
    });
  }

  render() {
    return (
      <div>
        <div className="News">
          <h3>{this.state.content}</h3>
        </div>
        <div className="footer">
          <h4>&copy;Copyright by Dragos Tothazan</h4>
        </div>
      </div>
    )
  }
}

export default News;

