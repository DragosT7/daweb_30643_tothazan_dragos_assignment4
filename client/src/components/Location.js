import React, { Component } from 'react';
import xmldom from 'xmldom';
import { Map, InfoWindow, Marker, GoogleApiWrapper, Polyline } from 'google-maps-react';


class Location extends Component {
    constructor(props) {
        super(props);
        this.state = {
            latitude: null,
            longitude: null,
            userAddress: null
        };
        this.getLocation = this.getLocation.bind(this);
        this.getCoordinates = this.getCoordinates.bind(this);
    }

    getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(this.getCoordinates, this.handleLocationError);
        } else {
            alert("Geologation is not suppoerted");
        }
    }

    getCoordinates(position) {
        this.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        })
    }

    handleLocationError(error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                alert("User denied the request for Geolocation.")
                break;
            case error.POSITION_UNAVAILABLE:
                alert("Location information is unavailable.")
                break;
            case error.TIMEOUT:
                alert("The request to get user location timed out.")
                break;
            case error.UNKNOWN_ERROR:
                alert("An unknown error occurred.")
                break;
            default:
                alert("An unknown error occurred.")

        }
    }

    render() {

        return (
            <div>
                <button onClick={this.getLocation}>Find me!</button>

                <Map google={this.props.google} zoom={3}>
                    <Marker
                        title={'Current address.'}
                        name={'Current address'}
                        position={{ lat: this.state.latitude, lng: this.state.longitude }} />

                    <Marker
                        title={'Contact address.'}
                        name={'VIVO CLUJ-NAPOCA'}
                        position={{ lat: 46.750212, lng: 23.533384 }}
                    />

                    <Polyline
                        path={[{ lat: 46.750212, lng: 23.533384 }, { lat: this.state.latitude, lng: this.state.longitude }]}
                        options={{
                            strokeOpacity: 1,
                            strokeWeight: 4,
                            offset: '0%',
                            icons: [
                                {
                                    strokeWeight: 2,
                                    offset: '0%',
                                    repeat: '35px'
                                }
                            ]
                        }}
                    />
                    <InfoWindow onClose={this.onInfoWindowClose}>

                    </InfoWindow>
                </Map>
            </div>
        )
    }

}

// export default LocationGetter

export default GoogleApiWrapper({
    apiKey: ("AIzaSyA1CJd-o2z9zseqwf4KpxyN8R-54C5MWWM")
})(Location)