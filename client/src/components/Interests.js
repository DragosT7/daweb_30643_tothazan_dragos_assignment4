import React from 'react';
import { useTranslation } from 'react-i18next';

function Interests() {
  const { t, i18n } = useTranslation();
  return (
    <div>
      <div class="Interests">
        <h3>{t('Interests.2')}:</h3>
        <ul className="ul-style"> 
          <li><h4>Python</h4></li>
          <li><h4>Django</h4></li>
          <li><h4>Django REST Framework</h4></li>
          <li><h4>Vagrant</h4></li>
        </ul>
      </div>
      <div className="footer">
        <h4>&copy;Copyright by Dragos Tothazan</h4>
      </div>
    </div>
  );
}

export default Interests;
