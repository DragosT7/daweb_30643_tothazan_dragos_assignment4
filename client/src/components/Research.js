import React from 'react';
import { useTranslation } from 'react-i18next';

function Research() {
    const { t, i18n } = useTranslation();
    return (
        <div>
            <div className="Research">
                <h3>{t('TeachersPublications.1')}:</h3>
                <ul>
                    <li>Pete Donnell, Murad Banaji, Anca Marginean, and Casian Pantea. CoNtRoL:
                    an open sourcework for the analysis of chemical reaction networks. BioInformatics,
                    30(11):1533-1634, 2014.
                    </li>
                    <li>Anca Marginean. GFMed: Question Answering over BioMedical Linked Data with Grammatical
                    Framework. In Linda Cappellato, Nicola Ferro, Martin Halvey, and Wessel Kraaij, editors,
                    Working Notes for CLEF 2014 Conference, Sheffield, UK, volume 1180 of CEUR Workshop
                    Proceedings, pp1224-1235, CEUR-WS.org, September 2014.
                    </li>
                    <li>
                        I.A. Letia, A.N. Marginean. Expectations for assessment of non functional requirements
                        of web services. In 12th International Symposiom on Symbolic and Numeric Algorithms for
                        Scientific Computing, Synasc 2010, Ed. T. Ida, V. Negru, T. Jebelean, D. Petcu, S. M. Watt,
                        D. Zaharie, Timisoara, Romania, pp 553-556, 2010.
                    </li>
                    <li>
                        A. N. Marginean, Blending Rules and Ontology in Argumentation, 31st International Conference
                        on Infomation Technology Interfaces, ISBN 978-953-7138-15-8; ISSN 1330-1012, 2009.
                    </li>
                    <li>
                        I.A. Letia, A.N. Marginean, Client Provider Collaboration for Service Bundling, In Advances
                        in Electrical and Computer Engineering, Suceava, Romania, ISSN 1582-7445, No 1/2008, vol 8.
                    </li>
                </ul>
            </div>
            <div className="footer">
                <h4>&copy;Copyright by Dragos Tothazan</h4>
            </div>
        </div>
    );
}

export default Research;
