import React from 'react';
import { useTranslation } from 'react-i18next';
import Location from './Location';
function Contact() {
  const { t, i18n } = useTranslation();
  return (
    <div>
      <div className="Contact">
        <h3>{t('Address.1')}: Cluj-Napoca</h3>
        <h3>{t('Phone.1')}: 0751 380 xxx</h3>
        <h3>Mail: tothazandragos7@gmail.com</h3>
      </div>
      <div className="footer">
        <h4>&copy;Copyright by Dragos Tothazan</h4>
      </div>
      <Location/>
    </div>
  );
}

export default Contact;
